import sys, os

#if sys.platform == "win32":
#    os.environ["SDL_VIDEODRIVER"] = "directx"
os.environ["PYGAME_FREETYPE"] = ""
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"

import pygame

import gamestate

#Hack pyconsolegraphics to use our custom backend
import pyconsolegraphics.backends.pygame as p

class TotWindowBackend(p.PygameSurfaceBackend):
    def initialize_surface(self, w, h):
        #ConsoleWindow gives it the window's surface
        self.surface = None

    def get_mouse(self, topleft = True):
        cursor_pos_in_window = pygame.mouse.get_pos() - self.window_topleft
        return self.px_to_cell(cursor_pos_in_window, topleft)

def totwindow(terminal):
    return TotWindowBackend(terminal)

import pyconsolegraphics
pyconsolegraphics.backends.backend_funcs = {"totwindow" : totwindow}
pyconsolegraphics.available_backends = {"totwindow"}

if __name__ == '__main__':
    pygame.init()
    pygame.key.set_repeat(1, 15)

    game = gamestate.GameState()
    game.play()