import functools
from collections import namedtuple
from math import floor, sin, cos, hypot, floor, ceil, atan2, radians

direction_symbol_map = {(0, 0): "∙", (0, -1): "↑", (0, 1): "↓", (1, 0): "→", (-1, 0): "←",
                            (1, 1): "↘", (-1, 1): "↙", (-1, -1): "↖", (1, -1): "↗"}

class V(namedtuple('V', ['x', 'y'])):
    """2D vector"""
    E = 1e-5

    def __new__(cls, x, y=None):
        try:
            return super().__new__(cls, *x)
        except TypeError:
            return super().__new__(cls, x, y)

    def __gt__(self, other):
        return self.x > other[0] and self.y > other[1]
    def __lt__(self, other):
        return self.x < other[0] and self.y < other[1]
    def __ge__(self, other):
        return self.x >= other[0] and self.y >= other[1]
    def __le__(self, other):
        return self.x <= other[0] and self.y <= other[1]

    def __add__(self, other):
        return V(self.x + other[0], self.y + other[1])

    def __sub__(self, other):
        return V(self.x - other[0], self.y - other[1])

    def __rsub__(self, other):
        return V(other[0] - self.x, other[1] - self.y)

    def __mul__(self, other):
        try:
            x2, y2 = other
        except TypeError:
            return type(self)(self[0] * other, self[1] * other)
        else:
            x1, y1 = self
            return (x1 * x2) + (y1 * y2)

    def __truediv__(self, other):
        return V(self.x / other, self.y / other)

    def __round__(self, n=None):
        return V(round(self.x, n), round(self.y, n))

    def __floor__(self):
        return V(floor(self.x), floor(self.y))

    def __floordiv__(self, other):
        return V(int(self.x//other), int(self.y//other))

    def __mod__(self, other):
        try:
            x, y = other
        except TypeError:
            x = y = other

        return V(self.x % x, self.y % y)

    def round_towards(self, other):
        """If other.x is positive, round self.x up, otherwise round down.
           If other.y is positive, round self.y up, otherwise round down.

           You can think of this as a "velocity" round, if you do
           grid_location = self.location.round_towards(self.velocity)
           it'll give you the nearest on-grid (rounded) point you were heading towards.
        """
        return V(
            ceil(self.x) if other.x > 0 else floor(self.x),
            ceil(self.y) if other.y > 0 else floor(self.y)
        )

    def angle_to(self, other):
        """
        Assuming this vector and the other are coincident lines
        return the degrees of separation between them.
             |
         self| theta
             |-----
               other
        """
        return atan2(other.y - self.y, other.x - self.x)

    def atan2(self):
        """Assuming this vector represents a direction, return the
        angular offset in radians this vector represents from (0, 1) (straight down)"""
        return atan2(self.x, self.y)

    def bearing(self, other):
        """Assuming this and other are points, return the
        angle you'd have to rotate a vector pointing straight down
        to get a vector pointing from this point to the the other."""
        return (other - self).atan2()

    def rotate(self, theta):
        """Return a vector of the same magnitude of this vector, but rotated anticlockwise so
        that it describes a line at an angle of theta to this vector.
        >>> V(0, 1).rotate(radians(90)).sanitized()
        V(-1, 0)[←]
        >>> V(0, 1).rotate(radians(-90)).sanitized()
        V(1, 0)[→]
        """
        sin_of_theta = sin(theta)
        cos_of_theta = cos(theta)
        x, y = self
        return V(x * cos_of_theta - sin_of_theta * y,
                 x * sin_of_theta + cos_of_theta * y)

    @classmethod
    def from_yaw(cls, yaw):
        """Return a vector yaw radians clockwise from (0,1) (straight down). I.e.
        >>> V.from_yaw(radians(90)).sanitized()
        V(1, 0)[→]"""
        return V(sin(yaw), cos(yaw))

    def magnitude(self):
        return hypot(*self)

    def distance(self, other):
        return (other - self).magnitude()

    def normalized(self):
        try:
            return self / self.magnitude()
        except ZeroDivisionError:
            return V(0,0)

    def sanitized(self):
        """Return this vector with components that are under 1e-5 rounded to 0. If either component's
        decimal part is 0, casts it to an int.

        >>> V(0.000001, 1).sanitized()
        V(0, 1)[↓]
        >>> V(1.000001, 1.000001).sanitized()
        V(1.000001, 1.000001)[↘]
        >>> V(5.0, 6.0).sanitized()
        V(5, 6)[↘]"""
        x, y = self
        if abs(x) < self.E: x = 0
        if abs(y) < self.E: y = 0
        if int(x) == x: x = int(x)
        if int(y) == y: y = int(y)
        return V(x, y)

    def to_character(self):
        """Return a Unicode arrow representing the direction of this vector to within 45 degrees (there's only eight
        arrow characters, plus a centered dot representing (0,0).

        >>> V(0, 1).to_character()
        '↓'
        >>> V(50,50).to_character()
        '↘'
        >>> V(0,0).to_character()
        '∙'
        >>> V(0.00001, 0).to_character()
        '→'
        """
        if self == (0, 0): return '∙'
        normself = self.normalized()
        mydirection = max(direction_symbol_map, key = normself.__mul__)
        return direction_symbol_map[mydirection]

    def clamped(self, lower=(0,0), upper=(float("inf"), float("inf"))):
        """Given a box whose top left corner is lower and bottom right corner
        is upper, return closest point to this pos inside this box. Min defaults
        to (0,0).

        >>> V(5, 5).clamped((10, 0), (20, 20))
        V(10, 5)[↘]
        >>> V(-5,-5).clamped(upper=(80, 40))
        V(0, 0)[∙]
        >>> V(10,10).clamped(upper=(10, 10))
        V(9, 9)[↘]
        """
        minx, miny = lower
        maxx, maxy = upper
        if minx > maxx or miny > maxy:
            raise ValueError("Negative range from {} to {}. Did you omit upper=?".format(min, max))
        x, y = self
        if   x > maxx - 1: x = maxx - 1
        elif x < minx:     x = minx
        if   y > maxy - 1: y = maxy - 1
        elif y < miny:     y = miny
        return V(x, y)

    @classmethod
    def centroid(cls, *vecs):
        """Return the mean of all given vectors, a point equally distant from each of them. If two vectors are given,
        representing a line, this will return a point halfway along the line. If three vectors are given, representing a
        triangle, this will return the center of the triangle, et cetera."""
        if len(vecs) == 1 and not isinstance(vecs, V): vecs, = vecs
        vecs = [V(x, y) for x, y in vecs]
        return V(functools.reduce(lambda a, b: a+b, vecs) / len(vecs))

    def midpoint(self, other):
        """Return the point halfway between this point and the other."""
        return self.centroid(self, other)

    def __repr__(self):
        return f"V({self.x}, {self.y})[{self.to_character()}]"