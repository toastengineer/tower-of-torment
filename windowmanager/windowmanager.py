import collections.abc
import inspect
import sys
import warnings

import pygame
import pyconsolegraphics

from vec2d import V

background_color = (14,55,47)

_attrs_to_pass_through_to_rect = {'x', 'y', 'top', 'left', 'bottom', 'right', 'topleft', 'bottomleft', 'topright', 'bottomright', 'midtop', 'midleft', 'midbottom', 'midright', 'center', 'centerx', 'centery', 'size', 'width', 'height', 'w', 'h'}
class GfxWindow:
    """A patch of screen real estate that can be moved around and on top of other things, enabling you to draw
    without having to worry about interfering with anything else on the screen."""

    accepts_focus = False
    def __init__(self, size = (600, 400), position = None, activate = True, subwindow_of: 'GfxWindow' = None, dbg_label = None):
        """Size is a 2-tuple of ints - width, height. Position is a 2-tuple of ints - x, y. A pygame Rect can be
        provided for size instead - in that case, the value of position will be ignored. dbg_label is an internal name
        that appears in the window's repr(). If Activate is false, the window will not immediately be shown when it is
        instantiated. subwindow_of specifies that this window exists inside another window and its backing surface
        is a subsurface of its parent's surface."""
        frame = 1
        while True:
            caller = inspect.getframeinfo(inspect.stack()[frame][0])
            #it's a subclass calling super().__init__ - we want to find the actual client
            if caller.function == "__init__" or caller.filename == __file__:
                frame += 1
            else: break
        self.created_at = f"{caller.filename}:{caller.lineno}"
        del caller

        if isinstance(size, pygame.Rect):
            self.rect = size
        else:
            if position is None:
                position = suggest_window_position(size)
            self.rect = pygame.Rect(position, size)

        self.subwindows: list[GfxWindow] = []
        if subwindow_of is None:
            self.surface = pygame.Surface(size)
            self.is_subwindow = False
            if activate: self.show()
        else:
            self.surface = subwindow_of.surface.subsurface(self.rect)
            self.is_subwindow = True
            subwindow_of.subwindows.append(self)

        self.dbg_label = dbg_label

    def decorate(self, surf: pygame.Surface, gamestate: 'gamestate.GameState'):
        """Draw the contents of the window. Note that subwindows don't get .decorate() calls from the window manager
        even when their parent window is active - it's up to the parent window to decide when to call .decorate, so it
        can draw and let its subwindows draw over it, draw over its subwindows, or both. The base class decorate
        fills the window with black and then calls .decorate on all its subwindows."""
        #surf.fill((0,0,0))
        for the_subwindow in self.subwindows:
            the_subwindow.decorate(the_subwindow.surface, gamestate)

    def __getattr__(self, attr):
        if attr in _attrs_to_pass_through_to_rect:
            val = getattr(self.rect, attr)
            if isinstance(val, tuple) and len(val) == 2:
                val = V(val)
            return val
        super().__getattribute__(attr)

    def __setattr__(self, attr, value):
        if attr in _attrs_to_pass_through_to_rect:
            if self.is_subwindow: raise AttributeError(f"Tried to move subwindow ({self}.{attr} = {value})")
            setattr(self.rect, attr, value)
            self.surface = pygame.Surface(self.rect)
        else:
            super().__setattr__(attr, value)

    def move(self, x, y = None):
        """Move the window by the given vector."""
        if y is None: x, y = x
        self.rect.move_ip(x, y)

    def inflate(self, x, y = None):
        """Grow/shrink the window by the given vector."""
        if y is None: x, y = x
        self.rect.inflate_ip(x, y)
        self.surface = pygame.Surface(self.rect)

    def subdivide(self, horizontally = 1, vertically = 1, subwin_class = None, **kwargs):
        """Return a list of sub-windows that act like normal GfxWindow-s but cannot be moved or scaled - they stay in
        their place inside their parent window."""
        if subwin_class is None: subwin_class = type(self)
        if kwargs: assert all(isinstance(e, collections.abc.Iterable) for e in kwargs.values()), "All keyword args to subdivide must be iterable"
        if horizontally == vertically == 1:
            if not kwargs: raise ValueError("Cannot subdivide in to 1 subwindow")
            horizontally = len(next(iter(kwargs.values())))

        subwin_width  = self.width // horizontally if horizontally else self.width
        subwin_height = self.height // vertically if vertically else self.height
        subwin_count  = horizontally * vertically

        each_subwins_kwargs = [{} for _ in range(subwin_count)]
        for kw, args in kwargs.items():
            for subwin_arg_dict, arg in zip(each_subwins_kwargs, args):
                subwin_arg_dict[kw] = arg

        assert len(each_subwins_kwargs) == subwin_count, f"Creating {horizontally}x{vertically}={subwin_count}" \
                                                         f" subwindows, but only provided with {len(each_subwins_kwargs)}" \
                                                         f" sets of kwargs."

        each_subwins_kwargs.reverse()

        subwins = []
        for x in range(0, horizontally):
            for y in range(0, vertically):
                rect = pygame.Rect((x * subwin_width, y * subwin_height), (subwin_width, subwin_height))
                the_subwin = subwin_class(rect, subwindow_of = self, **each_subwins_kwargs.pop())
                subwins.append(the_subwin)

        return subwins

    def show(self):
        """Make this window appear on screen and receive decorate() calls."""
        if self in visible_windows: raise ValueError(f"Window {self} is already active")
        visible_windows.append(self)

        global currently_focused_window
        if currently_focused_window is None:
            currently_focused_window = self

    def hide(self):
        """Make this window not appear on screen or recieve decorate() calls anymore."""
        if self not in visible_windows: raise ValueError(f"Window {self} is not active")
        visible_windows.remove(self)

        if currently_focused_window is self:
            focus_topmost_focusable_window()

    def __repr__(self):
        dbg_label = f", dbg_label = {self.dbg_label}" if self.dbg_label else ""
        if self.is_subwindow: dbg_label += " [is subwindow]"
        try: size = self.size
        except Exception: size = "[underlying surface is bad]"
        return f"{type(self).__name__}({size}, active = {self in visible_windows}{dbg_label})"

    def on_up_pressed(self):
        """Called when the up button (up arrow or W by default) is pressed."""
    def on_up_released(self):
        """Called when the up button is released."""
    def on_down_pressed(self):
        """Called when the down button (down arrow or S by default) is pressed."""
    def on_down_released(self):
        """Called when the down button is released."""
    def on_left_pressed(self):
        """Called when the left button (left arrow or A by default) is pressed."""
    def on_left_released(self):
        """Called when the left button is released."""
    def on_right_pressed(self):
        """Called when the right button (right arrow or D by default) is pressed."""
    def on_right_released(self):
        """Called when the right button is released."""
    def on_action_pressed(self):
        """Called when the action button (ENTER by default) is pressed."""
    def on_action_released(self):
        """Called when the action button is released."""
    def on_escape_pressed(self):
        """Called when the escape button (ESC by default) is pressed."""
    def on_escape_released(self):
        """Called when the escape button is released."""
    def on_debug_pressed(self):
        """Called when the debug key is pressed."""
    def on_debug_released(self):
        """Called when the debug key is released."""

    def on_left_mouse_pressed(self, loc: V):
        """Called when the user left-clicks inside the window. Loc is already transformed to be inside the window."""
    def on_right_mouse_pressed(self, loc: V):
        """Called when the user right-clicks inside the window. Loc is already transformed to be inside the window."""
    def on_left_mouse_released(self, loc: V):
        """Called when the user releases a left-click inside the window. Loc is already transformed to be inside the window."""
    def on_right_mouse_released(self, loc: V):
        """Called when the user releases a right-click inside the window. Loc is already transformed to be inside the window."""


    def on_character(self, character):
        """Called when any keyboard button is pressed. Character is a single unicode character, which will be
        capitalized if the user is holding the shift key. If, say, W is the forward button, and the user presses W,
        BOTH on_forward_pressed() and on_character('w') will be called."""


class ConsoleWindow(GfxWindow):
    """A window that is also a pyconsolegraphics terminal, used to show text."""
    def __init__(self, size = (600, 400), position = None, activate = True, subwindow_of = None, dbg_label = None):
        super().__init__(size, position, activate, subwindow_of, dbg_label)
        w, h = self.size
        w //= 8
        h //= 16
        self.terminal=pyconsolegraphics.Terminal((w, h), backend = "totwindow")
        self.terminal.backend.surface = self.surface
        self.terminal.backend.window_topleft = self.topleft

    def text_decorate(self, term):
        pass

    def decorate(self, surf: pygame.Surface, gamestate: 'gamestate.GameState'):
        self.text_decorate(self.terminal)
        self.terminal.process()
        self.terminal.draw()


class YesNoPopupWindow(ConsoleWindow):
    """Modal dialog with the given prompt and buttons for yes and no. Calls the yes or no callback (if provided)
    and deactivates itself when a button is pressed."""

    def __init__(self, prompt, yes_callback = lambda: None, no_callback = lambda: None, default = "no"):
        super().__init__((500,200))
        if default not in {"yes", "no"}: raise ValueError(f"Default must be 'yes' or 'no' (was {default})")

        import pyconsolegraphics.ui as ui
        import pyconsolegraphics.ezmode as ezmode

        ez = ezmode.EZMode(self.terminal)
        ez.center_line_at(prompt, "center")

        yes = ui.VisibleClickZone(self.terminal, self.terminal.bottomcenter + ( 5, -3), "Yes", hotkey = 'y')
        no  = ui.VisibleClickZone(self.terminal, self.terminal.bottomcenter + (-5, -3), "No", hotkey = 'n')

        self.terminal.clickzonemanager.generate_links("default")

        def wrapper(f):
            self.hide()
            f()

        yes.on_click = lambda: wrapper(yes_callback)
        no.on_click  = lambda: wrapper(no_callback)

        self.terminal.clickzonemanager.activate_group("default")

        if default == 'yes': self.terminal.clickzonemanager.highlight(yes)
        if default == 'no' : self.terminal.clickzonemanager.highlight(no)

def suggest_window_position(size):
    """Guess a good spot for a window of the given size, trying not to cover up any other windows."""
    rect = pygame.Rect((0,0), size)
    rect.center = 640, 360
    return rect.topleft

def reset():
    """For unit testing. Delete all windows, return everything to a 'just-initialized' state."""
    visible_windows.clear()

def focus_topmost_focusable_window():
    """Called by GfxWindow.hide if the window being hidden is focused, or by handle_input if there is no focused
    window. Focus the newest/topmost/furthest-right-in-visible_windows window that takes keyboard input."""
    global currently_focused_window
    if currently_focused_window is None:
        #If no window has focus, probably because the focused window closed, give focus to the topmost
        #window that can be focused
        for the_window in reversed(visible_windows):
            if the_window.accepts_focus:
                currently_focused_window = the_window
                break

def handle_input():
    """React to user input. Mostly dispatches input to windows, but also handles the user switching focus between
    windows, click-n'-dragging things between windows or between spots in the same window, and the OS sending us
    a quit event from, for example, the user pressing Alt-F4, or trying to close the game window.
    """
    global currently_focused_window

    if currently_focused_window is None:
        #If no window has focus, probably because the focused window closed, give focus to the topmost
        #window that can be focused
        focus_topmost_focusable_window()

    window_to_rect_map = {w: w.rect for w in visible_windows}

    for the_event in pygame.event.get():
        if the_event.type == pygame.QUIT:
            sys.exit()

        if currently_focused_window:
            if the_event.type == pygame.KEYDOWN:
                currently_focused_window.on_character(the_event.unicode)
                #TODO: Implement keybinds instead of hardcoding wasd-enter-esc
                if the_event.key == pygame.K_w:
                    currently_focused_window.on_up_pressed()
                elif the_event.key == pygame.K_a:
                    currently_focused_window.on_left_pressed()
                elif the_event.key == pygame.K_s:
                    currently_focused_window.on_down_pressed()
                elif the_event.key == pygame.K_d:
                    currently_focused_window.on_right_pressed()
                elif the_event.key == pygame.K_ESCAPE:
                    currently_focused_window.on_escape_pressed()
                elif the_event.key == pygame.K_RETURN:
                    currently_focused_window.on_action_pressed()
                elif the_event.key == pygame.K_F7:
                    currently_focused_window.on_debug_pressed()
            elif the_event.type == pygame.KEYUP:
                if the_event.key == pygame.K_w:
                    currently_focused_window.on_up_released()
                elif the_event.key == pygame.K_a:
                    currently_focused_window.on_left_released()
                elif the_event.key == pygame.K_s:
                    currently_focused_window.on_down_released()
                elif the_event.key == pygame.K_d:
                    currently_focused_window.on_right_released()
                elif the_event.key == pygame.K_ESCAPE:
                    currently_focused_window.on_escape_released()
                elif the_event.key == pygame.K_RETURN:
                    currently_focused_window.on_action_released()
                elif the_event.key == pygame.K_F7:
                    currently_focused_window.on_debug_released()

        if the_event.type in {pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP}:
            result = pygame.Rect(the_event.pos, (1,1)).collidedictall(window_to_rect_map, True)
            if not result: continue
            moused_window: GfxWindow = result[0][0]
            if moused_window.accepts_focus:
                currently_focused_window = moused_window
            loc = the_event.pos - moused_window.topleft

            if the_event.type == pygame.MOUSEBUTTONDOWN:
                if the_event.button == pygame.BUTTON_LEFT:
                    moused_window.on_left_mouse_pressed(loc)
                elif the_event.button == pygame.BUTTON_RIGHT:
                   moused_window.on_right_mouse_pressed(loc)
            elif the_event.type == pygame.MOUSEBUTTONUP:
                if the_event.button == pygame.BUTTON_LEFT:
                    moused_window.on_left_mouse_released(loc)
                elif the_event.button == pygame.BUTTON_RIGHT:
                    moused_window.on_right_mouse_released(loc)

def draw(surf, gamestate):
    """Call .decorate() on all active windows, then draw the windows to the given surface. """
    surf.fill(background_color)

    for the_window in visible_windows:
        if sys.getrefcount(the_window) == 2: #the list, and the_window here
            warnings.warn(f"Orphan window defined @ {the_window.created_at}", RuntimeWarning)

        try:
            the_window.decorate(the_window.surface, gamestate)
        except Exception as e:
            text = e.args[0]
            text += f" - window created @ {the_window.created_at}"
            e.args = text, *e.args[1:]
            raise

    for the_window in visible_windows:
        if the_window is currently_focused_window:
            border_color = (127, 127, 127)
        elif the_window.accepts_focus and the_window.rect.collidepoint(pygame.mouse.get_pos()):
            #Showing you can highlight it if you click
            border_color = (96, 96, 96)
        else:
            border_color = (64, 64, 64)

        pygame.draw.rect(surf, border_color, the_window.rect.inflate(4, 4), 2, 2)
        surf.blit(the_window.surface, the_window.rect)

    pygame.display.flip()

visible_windows: list[GfxWindow] = []
import typing
currently_focused_window: typing.Optional[GfxWindow] = None