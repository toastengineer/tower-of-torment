import itertools
import time
from math import *
import unittest
import random

import pygame

import windowmanager
from pygame.draw import *

#Run the pyconsolegraphics backend hack
import main

lorem = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pharetra diam sit amet nisl suscipit. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Neque aliquam vestibulum morbi blandit cursus risus at. Mus mauris vitae ultricies leo integer malesuada nunc. Id leo in vitae turpis massa. Dui ut ornare lectus sit amet est. Metus aliquam eleifend mi in. Ullamcorper velit sed ullamcorper morbi tincidunt. Mattis vulputate enim nulla aliquet. Quam id leo in vitae turpis massa sed elementum. Interdum consectetur libero id faucibus nisl tincidunt eget. Odio ut sem nulla pharetra diam sit amet nisl suscipit. Fermentum posuere urna nec tincidunt praesent. Egestas purus viverra accumsan in nisl nisi scelerisque.
Sed nisi lacus sed viverra tellus in hac habitasse platea. Mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan. Ante metus dictum at tempor commodo ullamcorper a. Eget dolor morbi non arcu risus quis. Dui vivamus arcu felis bibendum ut tristique et. A pellentesque sit amet porttitor eget dolor morbi non arcu. Pretium viverra suspendisse potenti nullam ac. Venenatis tellus in metus vulputate eu scelerisque felis imperdiet proin. Blandit aliquam etiam erat velit scelerisque. Rhoncus mattis rhoncus urna neque viverra justo nec ultrices dui. Sapien faucibus et molestie ac feugiat.
Facilisis magna etiam tempor orci. At lectus urna duis convallis convallis tellus id interdum. Fames ac turpis egestas sed tempus. Mattis vulputate enim nulla aliquet porttitor lacus. Risus nullam eget felis eget nunc lobortis. Sagittis purus sit amet volutpat consequat mauris nunc congue nisi. Maecenas pharetra convallis posuere morbi leo. Placerat in egestas erat imperdiet sed euismod nisi porta. Nunc vel risus commodo viverra maecenas accumsan lacus. Vitae sapien pellentesque habitant morbi tristique senectus et netus. Elementum tempus egestas sed sed risus pretium quam. Libero justo laoreet sit amet cursus. Nunc id cursus metus aliquam eleifend. Sit amet massa vitae tortor condimentum lacinia quis. Elit scelerisque mauris pellentesque pulvinar.
Sed pulvinar proin gravida hendrerit lectus a. Bibendum ut tristique et egestas quis ipsum suspendisse. Scelerisque viverra mauris in aliquam. Ullamcorper morbi tincidunt ornare massa eget egestas purus viverra accumsan. Condimentum vitae sapien pellentesque habitant morbi tristique. Platea dictumst quisque sagittis purus sit amet volutpat. Interdum velit euismod in pellentesque. Ornare arcu dui vivamus arcu felis bibendum ut tristique. Cras pulvinar mattis nunc sed blandit libero. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Lacinia quis vel eros donec ac odio tempor orci. Pellentesque dignissim enim sit amet venenatis urna cursus. In nisl nisi scelerisque eu ultrices vitae auctor eu. Nulla at volutpat diam ut. Ullamcorper dignissim cras tincidunt lobortis feugiat. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Dictumst quisque sagittis purus sit amet volutpat. Consequat nisl vel pretium lectus quam. Adipiscing vitae proin sagittis nisl rhoncus mattis.
Nulla facilisi morbi tempus iaculis urna id volutpat lacus. Faucibus vitae aliquet nec ullamcorper sit amet risus nullam. Augue lacus viverra vitae congue eu consequat ac felis. Dignissim suspendisse in est ante in nibh mauris cursus. Cursus eget nunc scelerisque viverra mauris. Facilisi cras fermentum odio eu. Erat velit scelerisque in dictum non. Eget duis at tellus at. Sollicitudin nibh sit amet commodo nulla. Vel turpis nunc eget lorem dolor sed viverra ipsum nunc. Porttitor lacus luctus accumsan tortor posuere. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Maecenas accumsan lacus vel facilisis volutpat est velit egestas. Et netus et malesuada fames ac turpis. Dolor morbi non arcu risus."""


class ColorChangingCircleWindow(windowmanager.GfxWindow):

    def __init__(self, *args, freqs = (6, 7, 8), **kwargs):
        self.freqs = freqs
        super().__init__(*args, **kwargs)

    def decorate(self, surf, gamestate):
        f = lambda k: floor(255 * max(0, sin(time.monotonic() * k)))
        pygame.draw.circle(surf, tuple(f(k) for k in self.freqs), (50, 50), 50)

class WindowManagerTests(unittest.TestCase):
    prompt = True

    def setUp(self):
        import sys, os
        #if sys.platform == "win32":
        #    os.environ["SDL_VIDEODRIVER"] = "directx"
        os.environ["PYGAME_FREETYPE"] = ""
        os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"

        import pygame

        pygame.init()
        pygame.display.set_mode((1280, 720))
        pygame.key.set_repeat(500, 75)

    def tearDown(self):
        display = pygame.display.get_surface()
        if self.prompt:
            clk = pygame.time.Clock()
            for t in range(60 * 5):
                windowmanager.draw(display, None)
                #pygame.event.pump()
                pygame.display.flip()
                clk.tick(60)

        windowmanager.reset()

    def test_gfx_window(self):
        win = ColorChangingCircleWindow(size = (100, 100))

    def test_console_window(self):
        class ConsoleWindowTest(windowmanager.ConsoleWindow):
            accepts_focus = True
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.terminal.write("What's your name? ")
                self.terminal.stdiocursor.currently_getting_line = True
                self.terminal.stdiocursor.return_pressed = False
                self.terminal.stdiocursor.activate()

            def text_decorate(self, surf: pygame.Surface):
                if self.terminal.stdiocursor.currently_active:
                    if self.terminal.stdiocursor.return_pressed:
                        print(f"Hello {self.terminal.stdiocursor.finish()}")
        ConsoleWindowTest((300, 100), (300, 360))
        ConsoleWindowTest((300, 100), (700, 360))


    def test_subwindows(self):
        bigwin = windowmanager.GfxWindow()
        freqs = [(random.randint(3, 6), random.randint(3, 6), random.randint(3, 6)) for t in range(4*3)]
        subwindows = bigwin.subdivide(horizontally = 4, vertically = 3, subwin_class = ColorChangingCircleWindow,
                                      freqs = freqs)

    def test_console_subwindows(self):
        bigwin = windowmanager.GfxWindow()
        lorems = lorem.split("\n")
        class LoremPrinter(windowmanager.ConsoleWindow):
            def __init__(self, *args, lorem, **kwargs):
                self.lorem = itertools.cycle(lorem)
                super().__init__(*args, **kwargs)

            def text_decorate(self, term):
                for t in range(5):
                    term.write(next(self.lorem))

        subwindows = bigwin.subdivide(subwin_class = LoremPrinter, lorem = lorems)

    def test_window_repr(self):
        win = windowmanager.GfxWindow(dbg_label = "Foo bar")
        r = repr(win)

        win.surface = None
        repr(win)
        win.hide()

    def test_yesno_popup(self):
        def yes(): print("Clicked yes")
        def no(): print("Clicked no")

        windowmanager.YesNoPopupWindow("Yes/no popup", yes, no)