from math import *
from pathlib import Path

import windowmanager
from dungeon.renderer import WorldSprite

from vec2d import V

class Entity(WorldSprite):
    img_path = Path()

    def __init__(self, location):
        super().__init__(str(self.img_path), location)

    @classmethod
    def place(cls, world):
        pass

    def on_player_touch(self, gamestate):
        pass

    def update(self, gamestate):
        pass

    def on_interact(self, gamestate):
        pass


class ExitDoor(Entity):
    height = 0.4
    direction = pi / 2
    img_path = Path("images", "exitdoor.png")
    def __init__(self):
        super().__init__((1.5, 1.1))

    def __str__(self):
        return "the exit door"

    @classmethod
    def place(cls, world):
        if world.floor_number == 0:
            world.entities.append(cls())

    def on_player_touch(self, gamestate):
        return
        def yes():
            gamestate.current_level = None

        def no():
            gamestate.camera.location = V(1.5, 1.5)

        windowmanager.YesNoPopupWindow("Leave the Tower? Any items left inside will be lost!", yes, no)

    def on_interact(self, gamestate):
        def yes():
            gamestate.current_level = None

        windowmanager.YesNoPopupWindow("Leave the Tower? Any items left inside will be lost!", yes)

class LadderUp(Entity):
    height = 1.25
    img_path = Path("images", "ladderup.png")
    def __init__(self, location, level):
        super().__init__(location)
        self._next_floor = None
        self.next_ladder = None
        self.floor_number = level.floor_number
        self.level = level

    def __str__(self):
        return f"a ladder up to Floor {self.floor_number + 1}"

    @classmethod
    def place(cls, world):
        loc = world.end_loc + V(0.5, 0.5)
        world.entities.append(cls(loc, world))

    @property
    def next_floor(self):
        if not self._next_floor:
            from dungeon import level
            self._next_floor = level.Level(self.floor_number + 1)
            next_ladder_loc  = self._next_floor.start_loc + V(0.5, 0.5)
            self.next_ladder = LadderDown(next_ladder_loc, self.level, self)
            self._next_floor.entities.append(self.next_ladder)

        return self._next_floor

    def on_interact(self, gamestate):
        gamestate.current_level = self.next_floor
        gamestate.party.location = self.next_ladder.location
        print(f"You climb the ladder to Floor {gamestate.current_level.floor_number}.")

class LadderDown(Entity):
    height = 0
    img_path = Path("images", "ladderdown.png")

    def __str__(self):
        return f"a ladder down to the previous floor"

    def __init__(self, location, prev_level, prev_ladder):
        self.prev_level = prev_level
        self.prev_ladder = prev_ladder
        super().__init__(location)

    def on_interact(self, gamestate):
        gamestate.current_level = self.prev_level
        gamestate.party.location = self.prev_ladder.location
        print(f"You climb down the ladder to Floor {gamestate.current_level.floor_number}.")

def _get_all_subclasses(the_class):
    retlist = [the_class]
    for the_subclass in the_class.__subclasses__():
        retlist.extend(_get_all_subclasses(the_subclass))
    return retlist

__all__ = [c.__name__ for c in _get_all_subclasses(Entity)]