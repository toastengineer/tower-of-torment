import networkx
import numpy

import dungeon.mapgen as mapgen
from vec2d import V

class MapGeneratorSelector:
    """Maps floor numbers to the ruleset that should be used to generate that floor."""

    def __init__(self, rules):
        self.rules = {}
        n = 0
        for the_rule, floors in rules:
            self.rules[n] = the_rule
            n += floors

        self.floor_numbers = list(self.rules.keys())

    def __getitem__(self, floor_number):
        lower_limit = 0
        upper_limit = len(self.floor_numbers)

        if floor_number >= self.floor_numbers[-1]:
            return self.rules[self.floor_numbers[-1]]

        while upper_limit != lower_limit + 1:
            idx = lower_limit + (upper_limit - lower_limit) // 2
            if   self.floor_numbers[idx] <= floor_number:
                lower_limit = idx
            elif self.floor_numbers[idx] > floor_number:
                upper_limit = idx
            else:
                lower_limit = idx
                break

        return self.rules[self.floor_numbers[lower_limit]]

#Generation rule, number of floors to generate with that rule.
#Goes in sequence, so we have 3 10x10 dfs mazes, then after that 2 of the next rule, etc...
floor_number_to_generation_func = MapGeneratorSelector((
        (mapgen.ten_by_ten_dfs_maze, 3),
        (mapgen.ten_by_ten_seventy_twenty_mix, 2),
        (mapgen.ten_by_ten_fifty_fifty_mix, 3),
        (mapgen.ten_by_ten_magicnumber, 3),
        (mapgen.ten_by_ten_random, 2),
        (mapgen.chaos, 5),
))

class Level:
    """A single floor of the dungeon. Composed of a 2D array of bools that represent whether
    each position in the level contains a wall, and the list of Entity-s that are currently
    active in that level."""

    def __init__(self, floor_number = 0):
        self.floor_number = floor_number
        self.map, self.graph = floor_number_to_generation_func[self.floor_number]()

        self.shortest_paths = dict(networkx.all_pairs_shortest_path(self.graph))
        longest_shortest_path = max([self.shortest_paths[a][b] for a in self.graph.nodes for b in self.graph.nodes],
                                    key = len)
        self.start_loc = longest_shortest_path[0]
        self.end_loc = longest_shortest_path[-1]
        self.open_locations = set(map(V, numpy.transpose((self.map != True).nonzero())))
        self.adjacent_open_cells = numpy.pad(numpy.sum((self.map[1:-1,0:-2]==0, self.map[1:-1, 2:]==0, self.map[0:-2, 1:-1]==0, self.map[2:, 1:-1]==0),
                                             dtype=numpy.int8, axis=0), 1, constant_values=(0,))
        self.map &= (self.adjacent_open_cells != 4) #make any closed cells surrounded by open cells open
        self.open_locations_sorted_by_adjacent_cells = sorted(self.open_locations,
                                                              key = lambda x: self.adjacent_open_cells[x])
        self.entities = []
        from dungeon import Entity
        for the_entity_class in Entity.__subclasses__():
            the_entity_class.place(self)

        #What will get .on_interact if the player presses the interact button
        self.interaction_target = None

    def update(self, gamestate):
        for thesprite in self.entities:
            thesprite.update(gamestate)

        #Sprites can kick us out of the map, so better check if it's still there
        if not gamestate.current_level:
            return

        player_loc = gamestate.party.location
        for the_entity in (s for s in self.entities if player_loc.distance(s.location) < 0.15):
            the_entity.on_player_touch(gamestate)


