import functools
import itertools
import math
import random

import networkx
import numpy

from vec2d import V

def neighbors(loc, worldshape):
    return {V(loc) + adj for adj in
            (V(2,0), V(0,2), V(-2,0), V(0,-2))
            if (0,0) <= (V(loc) + adj) < worldshape}

f = 0

directions = (V(2, 0), V(0, 2), V(-2, 0), V(0, -2))
def growing_tree(size, pickfunc = random.choice, loopiness = 0.0, startloc = None):
    if not isinstance(size, tuple): size = (size, size)
    w, h = size
    size = w * 2 - 1, h * 2 - 1
    world = numpy.ndarray(size, numpy.bool_)
    graph = networkx.Graph()
    world[:] = True

    if startloc is None: startloc = V(random.randint(0, w-1) * 2, random.randint(0, h-1) * 2)
    open = [startloc]
    visited = set()
    while open:
        the_loc = pickfunc(open)
        visited.add(the_loc)
        world[the_loc] = False

        candidate_neighbors = neighbors(the_loc, world.shape)
        candidate_neighbors -= visited

        if not candidate_neighbors:
            open.remove(the_loc)
            if random.random() <= loopiness:
                candidate_neighbors = neighbors(the_loc, world.shape)
            else:
                continue

        neighbor_to_connect = random.choice(list(candidate_neighbors))

        world[neighbor_to_connect] = False
        midpoint = (the_loc + neighbor_to_connect) // 2
        world[midpoint] = False

        networkx.add_path(graph, (the_loc, midpoint, neighbor_to_connect))

        if neighbor_to_connect not in visited:
            open.append(neighbor_to_connect)
            visited.add(neighbor_to_connect)

    pad_adjusted_graph = networkx.Graph((a + V(1,1), b + V(1,1)) for a, b in graph.edges)
    return numpy.pad(world, 1, constant_values=(True,)), pad_adjusted_graph

class RoundRobinMultiplexer:
    def __init__(self, switch_every, *args):
        self.channels = args
        self.switch_every = switch_every
        self. t = 0
    def __call__(self, *args, **kwargs):
        idx = math.floor(self.t//self.switch_every) % len(self.channels)
        print(idx, self.t)
        self.t+=1
        return self.channels[idx](*args, **kwargs)
    def __repr__(self):
        import inspect
        return ' '.join(inspect.getsource(f) for f in self.channels)

dfs = lambda x: x[-1]
bfs = lambda x: x[0]
fiftyfifty = lambda x: x[-1] if random.random() < 0.5 else x[0]
seventytwenty = lambda x: x[-1] if random.random() < 0.7 else x[0]
twentyseventy = lambda x: x[-1] if random.random() > 0.7 else x[0]
mid = lambda x: x[len(x)//2]
magicnumber = lambda x: x[int(len(x) * 0.845)]
rand = random.choice
sinlen = lambda x: x[math.floor(math.sin(len(x))*len(x))]
triangle = lambda x: x[int(random.triangular(0, 1, 0) * len(x))]
min_ = lambda x: min(x, key = lambda v: v.magnitude())
max_ = lambda x: max(x, key = lambda v: v.magnitude())

maze_styles = [dfs, bfs, triangle, magicnumber, min_, max_, mid, sinlen]
combos  = [RoundRobinMultiplexer(4, a, b) for a in maze_styles for b in maze_styles if a is not b]
combos2 = [functools.partial(
            lambda a, b, x: a(x) if random.random()<0.5 else b(x), a, b)
            for a in maze_styles for b in maze_styles]
maze_styles.extend(combos)
maze_styles.extend(combos2)
combos3 = [functools.partial(
            lambda a, b, x: a(x) if random.random()<0.5 else b(x), a, b)
            for a in combos for b in combos]
maze_styles.extend(combos3)
maze_style_cycler = itertools.cycle(maze_styles)

def ten_by_ten_dfs_maze():
    """Produces a simple-looking maze with long corridors that don't branch much. The first few levels are generated
    with only this algorithm, to ease players in to the game."""
    return growing_tree(5, pickfunc = dfs)

def ten_by_ten_seventy_twenty_mix():
    """Mostly uses the dfs rule, but mixes in bfs 20% of the time - like pure dfs but with more branches"""
    return growing_tree(5, pickfunc = seventytwenty)

def ten_by_ten_fifty_fifty_mix():
    """Mostly uses the dfs rule, but mixes in bfs 50% of the time - even mix between generating long snakey corridors
    and trying to branch off."""
    return growing_tree(5, pickfunc = fiftyfifty)

def ten_by_ten_magicnumber():
    """Uses the "magic number" pick rule, which tries to set a balance between extending existing corridors and
    branching off but in a different way."""
    return growing_tree(5, pickfunc = magicnumber)

def ten_by_ten_random():
    """Picks which next spot to extend the maze to at complete random, resulting in long corridors that branch
    chaotically from the center."""
    return growing_tree(5, pickfunc = rand)

def chaos():
    """Randomly picks, or mixes and matches growing tree pick rules, and randomly picks size and loopiness. Produces a
    wide variety of very distinct feeling levels."""
    picker = random.choice(maze_styles)
    size = random.randint(5, 15)
    return growing_tree(size, picker, random.random())