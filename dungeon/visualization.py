# importing networkx
import networkx as nx
# importing matplotlib.pyplot
import matplotlib.pyplot as plt

import level
import mapgen
while True:
    array, graph = mapgen.growing_tree(6, pickfunc = mapgen.RoundRobinMultiplexer(25,
                                                                                   mapgen.dfs,
                                                                                  mapgen.dfs,
                                                                                   mapgen.bfs,
                                                                                   mapgen.dfs
                                                                                   ),
                                       loopiness = 0.3)

    plt.subplot(211)
    nx.draw(graph)
    plt.subplot(212)
    plt.imshow(array, cmap="Blues")

    plt.show()
