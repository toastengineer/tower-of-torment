#Based off this tutorial:
#https://permadi.com/1996/05/ray-casting-tutorial-table-of-contents/

from math import *

import time
import numpy
import pygame
import numba

from vec2d import V

def clamp_angle_for_comparison(unclamped_angle):
    halfpi = pi / 2
    unclamped_angle += halfpi
    unclamped_angle %= pi
    unclamped_angle -= halfpi
    return unclamped_angle

class Camera:
    """Viewpoint from which the world is drawn."""
    def __init__(self, location = (1.5, 1.5), rotation = 0, rot_degrees = None):
        self.location = V(location)
        self.rotation = radians(rot_degrees) if rot_degrees is not None else rotation

class WorldSprite:
    """An object that exists in the world at a certain location, represented by a 2D sprite."""
    height = 0.5
    direction = None #if this is not None, the sprite is drawn flat as if it was facing this yaw
    def __init__(self, imgpath, location):
        self.img = pygame.image.load(imgpath)
        self.imgpath = imgpath
        self.location = V(location)

    def __repr__(self):
        return f"{type(self).__name__}({self.imgpath}, {self.location})"

class LevelRenderer:
    """Draws the level from its camera's viewpoint to a buffer."""

    def __init__(self, camera, world = None, surface: pygame.Surface = None):
        self.camera = camera
        self.change_level(world)
        self.surf = surface

        self.w, self.h = surface.get_size()
        self.zbuffer = numpy.zeros(self.w, dtype=numpy.float_)

        self.fov = radians(75)

        self.projection_plane_distance = 0.000001

        self.radians_per_pixel = self.fov / self.w
        self.pixels_per_radian = self.w / self.fov

        self.ray_length = 10

        self.debug_view_scale = 40
        self.debug = False

    def change_level(self, world):
        if world is None:
            self.map = self.sprites = self.visibility = None
        else:
            self.map = world.map
            self.sprites = world.entities
            self.visibility = numpy.ndarray(V(self.map.shape) * 100, numpy.bool_)

    @staticmethod
    @numba.jit(nopython=True)
    def cast_ray_naive(world, visibility, check_x, check_y, ray_length, ray_x, ray_y):
        ray_x /= 125
        ray_y /= 125
        maxx, maxy = world.shape
        for t in range(ray_length * 125):
            check_x += ray_x
            if not 0 <= floor(check_x) < maxx:
                return floor(check_x), check_y, True

            if world[floor(check_x), floor(check_y)]:
                # It'll be some ways in to the blocked of square -
                # this moves the point on the grid line, to where it actually
                # intersected the wall
                check_x = round(check_x)
                break

            check_y += ray_y
            if not 0 <= floor(check_y) < maxy:
                return floor(check_y), check_y, True

            if world[floor(check_x), floor(check_y)]:
                check_y = round(check_y)
                break

            visibility[int(check_x * 100), int(check_y * 100)] = True

        return check_x, check_y, False

    def cast_ray_dda(self, worldMap, visibility, posX, posY, ray_length, rayDirX, rayDirY):
        deltaDistX = 1e30 if (rayDirX == 0) else abs(1 / rayDirX)
        deltaDistY = 1e30 if (rayDirY == 0) else abs(1 / rayDirY)

        mapX = int(posX)
        mapY = int(posY)

        #Ray started inside a wall, distance is 0
        if worldMap[mapX, mapY]:
            return 0, False

        outOfBounds = False

        if rayDirX < 0:
            stepX = -1
            sideDistX = (posX - mapX) * deltaDistX
        else:
            stepX = 1
            sideDistX = (mapX + 1 - posX) * deltaDistX

        if rayDirY < 0:
            stepY = -1
            sideDistY = (posY - mapY) * deltaDistY
        else:
            stepY = 1
            sideDistY = (mapY + 1 - posY) * deltaDistY

        while True:
            if sideDistX < sideDistY:
                sideDistX += deltaDistX
                mapX += stepX
                side = 0
            else:
                sideDistY += deltaDistY
                mapY += stepY
                side = 1

            try:
                if worldMap[mapX, mapY]:
                    break
            except IndexError:
                outOfBounds = True
                break

        if side == 0:
            perpWallDist = sideDistX - deltaDistX
        else:
            perpWallDist = sideDistY - deltaDistY

        return perpWallDist, outOfBounds

    def location_is_visible(self, location: V):
        return True#self.visibility[floor(location * 100)]

    def draw(self):
        if self.map is None: raise ValueError("no map")
        self.surf.lock()
        self.surf.fill((0,0,0))
        debug = self.debug

        #Draw the world top-down for debugging...
        if debug:
            for loc, opaque in ((V(x, y), self.map[x, y]) for x in range(self.map.shape[0]) for y in range(self.map.shape[1])):
                rect = pygame.Rect(tuple(loc * self.debug_view_scale), (self.debug_view_scale,self.debug_view_scale))
                pygame.draw.rect(self.surf, (127,127,127), rect, 0 if opaque else 1)

        self.visibility[:] = False

        camera_location = self.camera.location
        angular_resolution = self.radians_per_pixel
        ray_length = self.ray_length

        #self.projection_plane_distance = time.monotonic() - floor(time.monotonic())

        projection_plane_start = camera_location + V.from_yaw(self.camera.rotation - (self.fov / 2)) * self.projection_plane_distance
        projection_plane_end   = camera_location + V.from_yaw(self.camera.rotation + (self.fov / 2)) * self.projection_plane_distance
        projection_plane_length = projection_plane_start.distance(projection_plane_end)
        projection_plane_center = projection_plane_start.midpoint(projection_plane_end)
        projection_plane = projection_plane_end - projection_plane_start

        for x in range(self.w):
            position_of_pixel_on_projection_plane = projection_plane_start + (projection_plane * (x / self.w))
            ray_direction = (position_of_pixel_on_projection_plane - camera_location).normalized()
            dist, out_of_bounds = self.cast_ray_dda(self.map, self.visibility, *camera_location, ray_length, *ray_direction)
            hitpos = camera_location + ray_direction * dist
            #dist = position_of_pixel_on_projection_plane.distance(hitpos)

            #draw the rays if we're debugging
            if debug:
                pygame.draw.line(self.surf, (64,64,64) if not out_of_bounds else (128, 64, 64),
                                 camera_location * self.debug_view_scale,
                                 hitpos * self.debug_view_scale)
                #pygame.draw.circle(self.surf, (int(255 * (x / self.w)),255,0), position_of_pixel_on_projection_plane * self.debug_view_scale, 1)

            #if out_of_bounds: dist = inf
            #else: dist = camera_location.distance(hitpos)

            dist *= cos(self.camera.rotation - ray_direction.atan2())
            self.zbuffer[x] = dist
            #dist *= (1 - ((1 + sin(theta * 5 + (time.monotonic() / 5) )) / 10)) #'walls breathing' effect
            if dist == 0: continue
            idist = 1 - (dist / self.ray_length)
            if isfinite(idist):
                l = 1 / dist * 600
                mid = self.h//2
                shade = int(255 * idist)
                if not 0 <= shade <= 255:
                    shade = min(max(shade, 0), 255)
                shade_rounded = int(shade ** 2 * 0.00385)
                if not debug:
                    try:
                        pygame.draw.line(self.surf, (shade_rounded, shade_rounded, shade_rounded),
                                         (x, mid - l/2), (x, mid + l//2))
                    except AttributeError:
                        print((x, mid - l/2), (x, mid + l//2))

        # if self.debug:
        #     for loc in numpy.transpose(self.visibility.nonzero()):
        #         tenthscale = 2 #??????
        #         loc = V(loc) * tenthscale
        #         rect = pygame.Rect(tuple(loc * tenthscale), (tenthscale, tenthscale))
        #         pygame.draw.rect(self.surf, (0, 200, 0, 50), rect)

        if debug:
            #pygame.draw.circle(self.surf, (50, 127, 50), camera_location * self.debug_view_scale,
            #                   self.debug_view_scale // 10)
            ply_arc_rect = pygame.Rect(0,0, self.debug_view_scale // 5, self.debug_view_scale // 5)
            ply_arc_rect.center = (camera_location * self.debug_view_scale)
            pygame.draw.arc(self.surf, (50, 255, 50), ply_arc_rect, radians(270) + self.camera.rotation - (self.fov / 2), radians(270) + self.camera.rotation + (self.fov / 2))
            pygame.draw.line(self.surf, (127, 200, 127), camera_location * self.debug_view_scale, projection_plane_center * self.debug_view_scale)
            pygame.draw.line(self.surf, (127, 200, 127), projection_plane_start * self.debug_view_scale, projection_plane_end * self.debug_view_scale)

        self.surf.unlock()

        self.draw_sprites()

    def draw_sprites(self):
        """Draw all the worldsprites that are running around in the level on top of the level itself."""
        visible_sprites = [s for s in self.sprites if self.location_is_visible(s.location)]
        visible_sprites.sort(key = lambda s: self.camera.location.distance(s.location), reverse=True)

        if self.debug:
            for thesprite in self.sprites:
                rect = pygame.Rect((0, 0), (self.debug_view_scale, self.debug_view_scale))
                rect.center = floor(thesprite.location * self.debug_view_scale)
                self.surf.blit(
                    pygame.transform.smoothscale(thesprite.img,
                                                 (floor(self.debug_view_scale), floor(self.debug_view_scale))),
                                                 rect)
            return

        for thesprite in visible_sprites:
            angle_to_sprite = self.camera.location.bearing(thesprite.location)
            distance_to_sprite = self.camera.location.distance(thesprite.location)
            distance_to_sprite *= (cos(self.camera.rotation - angle_to_sprite) * 1)
            if distance_to_sprite <= 0.01: continue

            if thesprite.direction is not None:
                angle_to_sprite = clamp_angle_for_comparison(angle_to_sprite)
                perspective_width_factor = 1 - abs(angle_to_sprite / (pi / 2))
            else:
                perspective_width_factor = 1

            camera_relative_angle = (self.camera.rotation - angle_to_sprite)
            camera_relative_angle = clamp_angle_for_comparison(camera_relative_angle)

            screenx = ((self.fov/2) - camera_relative_angle) * self.pixels_per_radian
            if not 0 < screenx < self.surf.get_width() - 1: continue
            l = 1 / distance_to_sprite * 600
            mid = self.h // 2
            floorheight    = mid + l/2
            ceilingheight  = mid - l/2
            vertical_range = ceilingheight - floorheight
            screeny = int(floorheight + vertical_range * thesprite.height)

            #pygame.draw.circle(self.surf, (64, 64, 255), (round(screenx), screeny), 8)
            l = min(l, 1024)
            try:
                scaledsprite = pygame.transform.smoothscale(thesprite.img,
                                                            (min(int(l * perspective_width_factor), 2048),
                                                             min(int(l), 2048)))
            except pygame.error:
                import traceback
                print(traceback.format_exc())
                continue

            rect = scaledsprite.get_rect()

            try:
                rect.center = screenx, screeny
            except TypeError:
                print(f"sprite way too huge x = {screenx}, y = {screeny}")

            #Find unoccluded area - scan left, then right from center of sprites
            #This only works because of the shape of this game's levels, if it was possible to have something like this:
            #wwwwsssswwsssswwww
            #Where w is wall and s is sprite, this method would not work
            left_bound = min(max(floor(rect.right), 0), self.surf.get_width() - 1)

            #If we start in a wall, move off it
            while left_bound > rect.left:
                if self.zbuffer[left_bound] > distance_to_sprite: break
                left_bound -= 1
            else:
                #If we never find a spot on the sprite that is not behind a wall, sprite is fully occluded
                continue
            #Find first spot to the left of that that is occluded again - this is left edge of unoccluded area
            while left_bound > rect.left and self.zbuffer[left_bound] > distance_to_sprite:
                left_bound -= 1
            left_bound += 1

            #Do the same for the other side
            right_bound = max(0, min(ceil(rect.left), self.surf.get_width() - 1))
            while right_bound < rect.right and right_bound < self.surf.get_width():
                if self.zbuffer[right_bound] > distance_to_sprite: break
                right_bound += 1
            else: continue
            while right_bound < rect.right and right_bound < self.surf.get_width() and self.zbuffer[right_bound] > distance_to_sprite:
                right_bound += 1

            #pygame.draw.circle(self.surf, (0, 0, 255), (left_bound, self.surf.get_height() // 2), 5)
            #pygame.draw.circle(self.surf, (255, 0, 0), (right_bound, self.surf.get_height() // 2), 5)

            area = pygame.Rect((left_bound, 0), (right_bound - left_bound, self.surf.get_height()))

            #pygame.draw.rect(self.surf, (0,255,0), area)

            self.surf.set_clip(area)
            self.surf.blit(scaledsprite, rect)
            self.surf.set_clip(None)
