import unittest

import level

class LevelTests(unittest.TestCase):

    def test_level_generator_picker(self):
        """Assert that the level generator picker correctly interprets the list of rules and numbers of times to apply
        them that it's instantiated with, and gives the right rule for every floor number."""
        selector = level.MapGeneratorSelector((
            ("first_five", 5),
            ("next_three", 3),
            ("then_ten", 10),
            ("middle_ones", 25),
            ("final_boss", 1),
            ("post_game", 1),
        ))

        levels = [selector[n] for n in range(50)]
        correct_sequence= [
                "first_five", "first_five", "first_five", "first_five", "first_five",
                "next_three", "next_three", "next_three",
                "then_ten", "then_ten", "then_ten", "then_ten", "then_ten",
                "then_ten", "then_ten", "then_ten", "then_ten", "then_ten",
                "middle_ones", "middle_ones", "middle_ones", "middle_ones", "middle_ones",
                "middle_ones", "middle_ones", "middle_ones", "middle_ones", "middle_ones",
                "middle_ones", "middle_ones", "middle_ones", "middle_ones", "middle_ones",
                "middle_ones", "middle_ones", "middle_ones", "middle_ones", "middle_ones",
                "middle_ones", "middle_ones", "middle_ones", "middle_ones", "middle_ones",
                "final_boss",
                "post_game", "post_game", "post_game", "post_game", "post_game", "post_game"
        ]
        self.assertListEqual(levels, correct_sequence)

    def test_generate_some_levels(self):
        """Just generate a bunch of levels to see if an exception shakes out."""
        for t in range(105):
            generator = level.floor_number_to_generation_func[t].__name__
            print(f"Generating level {t} with {generator}")
            level.Level(t)
            print(f"Level {t} ok")
