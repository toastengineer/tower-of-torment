import pygame

import dungeon
import windowmanager

from vec2d import V

class LevelRenderWindow(windowmanager.GfxWindow):
    """UI window for interacting with the game world. Draws the 3D world to itself with a LevelRenderer, and the user
    directs the player characters to interact with the world through this window."""
    #When you move your character with a move key, the mouse cursor disappears. When we're in 'move mode,' right-clicking
    #or pressing the "interact button" (tab? space?) interacts with the closest thing we're looking at. If you move the
    #mouse, the cursor reappears, and now right-clicking doesn't do that - you can right click on an object in the 3D
    #view to interact with it, or click on other windows.
    accepts_focus = True

    def __init__(self, gamestate):
        super().__init__((640*2, 480))
        self.gamestate = gamestate
        self.party = gamestate.party
        self.renderer = dungeon.LevelRenderer(gamestate.party, surface = self.surface)
        self.last_level = None
        self.interact_key_target = None

    def decorate(self, surf, gamestate):
        level = gamestate.current_level
        level_id = id(level)
        if level_id != self.last_level:
            self.renderer.change_level(level)
            self.last_level = level_id

        self.renderer.draw()

        player_loc = gamestate.party.location
        #Get the renderer to remember which entities are visible instead of iterating over all entities
        interaction_candidates = (s for s in level.entities if
                                  self.renderer.location_is_visible(s.location) and
                                  player_loc.distance(s.location) <= 4)
        last_interaction_target = self.interact_key_target
        try:
            self.interact_key_target = min(interaction_candidates,
                                           key = lambda s: player_loc.distance(s.location))
            if self.interact_key_target != last_interaction_target:
                print(f"You see {self.interact_key_target}.")
        except ValueError:  #no candidates
            self.interact_key_target = None

    def on_up_pressed(self):
        camera = self.renderer.camera
        camera.location += V.from_yaw(camera.rotation) * 0.05

    def on_down_pressed(self):
        camera = self.renderer.camera
        camera.location += V.from_yaw(camera.rotation) * -0.05

    def on_left_pressed(self):
        camera = self.renderer.camera
        camera.rotation -= 0.025

    def on_right_pressed(self):
        camera = self.renderer.camera
        camera.rotation += 0.025

    def on_action_released(self):
        if self.interact_key_target:
            self.interact_key_target.on_interact(self.gamestate)

    def on_debug_released(self):
        self.renderer.debug = not self.renderer.debug

    def on_character(self, character):
        if character == '[':
            self.renderer.debug_view_scale -= 1
        elif character == ']':
            self.renderer.debug_view_scale += 1
        elif character == '=':
            self.renderer.projection_plane_distance += 0.001
        elif character == '-':
            self.renderer.projection_plane_distance -= 0.001
        elif character == 'q':
            from math import pi
            self.party.location += V.from_yaw(self.party.rotation - pi/2) * 0.001
        elif character == 'e':
            from math import pi
            self.party.location += V.from_yaw(self.party.rotation + pi / 2) * 0.001
        elif character == 'r':
            self.renderer.fov += 0.1
        elif character == 'f':
            self.renderer.fov -= 0.1

class Party(dungeon.Camera):
    """Represents the position and composition of the pack of characters the player is currently controlling."""
    pass

class GameState:
    """The overall global state of the game."""

    def __init__(self):
        self.current_level = dungeon.Level()
        self.party = Party()
        self.main_surface = None

    def update(self, dt = 1/60):
        """Step the simulation of the game world forward."""
        self.current_level.update(self)

    def draw(self):
        """Draw the game to the screen."""
        windowmanager.handle_input()
        windowmanager.draw(self.main_surface, self)

    def play(self):
        """Make the game go. Doesn't return."""
        self.main_surface = pygame.display.set_mode((1280, 720))
        LevelRenderWindow(self)
        while True:
            self.update()
            self.draw()

#Next thing to do is controls, window manager needs a notion of which window is in focus, then windows can have
#on_whatever methods